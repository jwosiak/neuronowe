import numpy as np
import matplotlib.pyplot as plt

def bern(n, k, p):
#    print  pow(p, k), pow(1.0 - p, n - k), pow(p, k) * pow(1.0 - p, n - k)
    return pow(p, k) * pow(1.0 - p, n - k)

def L(p, x, n):
    res = 1.0
    for i in np.arange(1, n + 1, 1):
        res *= bern(n, x, p)
 #   print res
    return res

n = 30

a = np.random.binomial(n, 0.4, n)

ps = np.linspace(0.0, 1.0, 1000)
b = np.zeros(ps.shape[0], dtype=float)

mean = float(np.sum(a)) / float(n)

for i in np.arange(ps.shape[0]):
    b[i] = L(ps[i], mean, n)


plt.plot(ps, b)

plt.show()

