import numpy as np
def p(C, D, prior):
	table = [[0.08167, 0.12702, 0.06966, 0.07507, 0.02758, 0.01974, 0.59926], [0.10503, 0.07352, 0.08328, 0.02445, 0.02062, 0.03206, 0.66104], [0.11525, 0.12181, 0.06247, 0.08683, 0.02927, 0.01008, 0.57429]]
	result = 1.0
	for r in l(D):
		result *= table[C][r]
	return prior*result

def l(D):
	res = []
	for letter in D:
		r = res
		if letter == 'a':
			res += [0]
		if letter == 'e':
                        res += [1]
		if letter == 'i':
                        res += [2]
		if letter == 'o':
                        res += [3]
		if letter == 'u':
                        res += [4]
		if letter == 'y':
                        res += [5]
		if r == res:
			res += [6]
	return res


def rec(prior):
	lang = ["E", "P", "S"]

	for D in ["bull", "burro", "kurczak", "pollo", "litwo, ojczyzno moja, ty jestes jak zdrowie", "dinero", "mama just killed a man put a gun against his head", "maradona es mas grande que pele"]:
		print "D =", D
		a = p(0, D, prior[0])
		b = p(1, D, prior[1])
		c = p(2, D, prior[2])
		print "p(E|D) =", a
		print "p(P|D) =", b
		print "p(S|D) =", c
		num = 0
		if max(a,b,c) == b:
			num = 1
		if max(a,b,c) == c:
			num = 2
		print "max prob =", lang[num], "\n"
	
print "B:"
rec([1.0/3.0, 1.0/3.0, 1.0/3.0])
print "\n\n"

print "C:"
rec([0.5, 0.2, 0.3])

