import numpy as np
from scipy import stats
from sklearn import datasets

def KNN(trainSet, testSet):
    Y = np.zeros((testSet.shape[0], trainSet.shape[0]), dtype=float)
    for i in np.arange(testSet.shape[0]):
        Y[i] = np.sqrt(np.sum(pow(np.array(trainSet - testSet[i], dtype=float), 2), axis=1))
    return np.argsort(Y, 1)


import random as rnd

iris = datasets.load_iris()
testSize = 50

# weights = np.mean(iris.data, axis=0)
# m = np.max(weights)
# weights = (np.ones(4, dtype=float) / weights) * m
# print weights
#
# iris.data *= weights
# print iris.data

attempts = 500
maxK = 21
errors = np.zeros(maxK, dtype=float)


for i in np.arange(attempts):

    allIndices = np.arange(iris.data.shape[0])
    testSamples = np.array(rnd.sample(allIndices, testSize))
    trainSamples = np.array(list(set(allIndices) - set(testSamples)))

    X = KNN(iris.data[trainSamples], iris.data[testSamples])
    Z = (iris.target[trainSamples])[X]

    for k in np.arange(1, maxK, 2):
        C = stats.mode(Z[:,:k], axis=1)[0].ravel() #- iris.target[testSamples]
        D = C - iris.target[testSamples]
        errors[k] += np.sum(D != 0)

results = (errors / (np.ones(maxK, dtype=float) * attempts*testSamples.shape[0]) )[np.arange(1,maxK, 2)]

# for i in np.arange(testSamples.shape[0])
#     if testSamples

import matplotlib.pyplot as plt

plt.plot(np.arange(1,maxK,2), results, 'bx', np.arange(1,maxK,2), results, 'b')
plt.show()
