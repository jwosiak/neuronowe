import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets
import matplotlib.pylab as pylab

params = {'legend.fontsize': 'x-small',
          'figure.figsize': (8, 8),
          'axes.labelsize': 'x-small',
          'axes.titlesize':'x-small',
          'xtick.labelsize':'x-small',
          'ytick.labelsize':'x-small'}
pylab.rcParams.update(params)

iris = datasets.load_iris()
f, axarr = plt.subplots(4, 4)

k1 = 0

for rowLab in iris.feature_names:
    rowData = iris.data[:,iris.feature_names.index(rowLab)]
    # plt.xlabel(colLab)
    k2 = 0
    axarr[k1, 0].set_ylabel(iris.feature_names[k1])
    for colLab in iris.feature_names:
        # plt.ylabel(rowLab)
        colData = iris.data[:, iris.feature_names.index(colLab)]

        for target in set(iris.target):
            example_ids = target==iris.target
            if  colLab == rowLab:
                axarr[k1,k2].hist(rowData, 10, normed=0, facecolor='blue', alpha=0.75, edgecolor="black")
            else:
                axarr[k1,k2].scatter(colData[example_ids], rowData[example_ids],
                        label=iris.target_names[target], color='bgr'[target],
                        marker='.', alpha=0.7)
                # axarr[k1, k2].plot(colData[example_ids], rowData[example_ids])
        k2 = k2 + 1
        axarr[3, k1].set_xlabel(iris.feature_names[k1])
    k1 = k1 + 1


axarr = axarr.T

x = np.arange(3)
b1 = plt.bar(x, x, bottom=-100, color='b')
b2 = plt.bar(x, x[::-1], bottom=-100, color='g')
b3 = plt.bar(x, -x, bottom=100, color = 'r')
f.legend([b1[0], b2[0], b3[0]], ['setosa', 'versicolor', 'virginica'], loc='upper center')

plt.show()
