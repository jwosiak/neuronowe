import numpy as np
from scipy import stats
from sklearn import datasets

def KNN(point, k, X, targets):
    Y = X - point
    Y = np.sqrt(np.sum(Y * Y, axis=1))
    Y = np.array(zip(Y, targets), dtype=[('x', float), ('y', int)])
    Y.sort(order='x')
    Y = zip(*Y)[1]
    return int(stats.mode(Y[:k])[0][0])
