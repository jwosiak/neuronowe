import pandas
import matplotlib.pyplot as plt
import numpy as np
from itertools import product
from StringIO import StringIO
import matplotlib.cm as cm

from sklearn import datasets
iris = datasets.load_iris()


def scatter_matrix(df,columns=None,group_col=None, cmap=None,alpha=0.5,normed=True):

    """Create a scatter matrix.

    Only plots those columns of datatype int or float.
    The color scheme is specified by cmap in a ScalarMappable

    Parameters
    ----------

    columns: list-like, default is None
        List of columns to restrict

    group_col: string, default is None
        The column to use group

    cmap: colors.Colormap, default = matplotlib.cm.jet
        The colormap to use

    alpha: float, default = 0.5
        The opacity to use for the plot

    normed: bool, default = True
        Value passed to matplotlib.pyplot.hist

    Returns
    -------

    2d array of Axes instances as per matplotlib.pyplot.subplots

    """

    if columns is None:
        columns = [col for col in df.columns
                   if df[col].dtype in (int,float)]

    df_scatter = df.filter(items=columns)

    n = len(columns)

    sm = cm.ScalarMappable(cmap=cmap)

    if group_col is None:
        groups = {'Data':df_scatter}
    else:
        s = df[group_col]
        groups = dict([(label,x) for label,x in\
                        df_scatter.join(s).groupby(group_col)])

    colors = dict(zip(groups.keys(),sm.to_rgba(np.arange(len(groups)))))
    fig, axs = plt.subplots(nrows=n,ncols=n)
    lines = {}

    for i,j in product(xrange(n),repeat=2):
        ax = axs[i,j]
        for label, group in groups.iteritems():

            #Align x, y, groups
            X = group.filter(items=set((df_scatter.columns[i],
                                        df_scatter.columns[j],
                                        group_col)))
            X = X.dropna()

            x = X[df_scatter.columns[i]]
            color = colors[label]

            if j == 0:
                ax.set_ylabel(df_scatter.columns[i])
            if i == 0:
                ax.set_title(df_scatter.columns[j])

            if i != j:
                y = X[df_scatter.columns[j]]
                ax.scatter(x,y,color=color,alpha=alpha)
            else:
                if len(x) > 0:
                    n,bins,patches = ax.hist(x,color=color,alpha=alpha)
                    if i == 0:
                        lines[label] = patches[0]

    if len(groups) > 1:
        leg = fig.legend(lines.values(),lines.keys(),ncol=len(groups),loc=(0,0))
        leg.draggable()

    return axs


# df = iris

if __name__ == "__main__":
    iris=\
    """sepal.length,sepal.width,petal.length,petal.width,class
    5.1,3.5,1.4,0.2,setosa
    4.9,3.0,1.4,0.2,setosa
    4.7,3.2,1.3,0.2,setosa
    4.6,3.1,1.5,0.2,setosa
    5.0,3.6,1.4,0.2,setosa
    5.4,3.9,1.7,0.4,setosa
    4.6,3.4,1.4,0.3,setosa
    5.0,3.4,1.5,0.2,setosa
    4.4,2.9,1.4,0.2,setosa
    4.9,3.1,1.5,0.1,setosa
    5.4,3.7,1.5,0.2,setosa
    4.8,3.4,1.6,0.2,setosa
    4.8,3.0,1.4,0.1,setosa
    4.3,3.0,1.1,0.1,setosa
    5.8,4.0,1.2,0.2,setosa
    5.7,4.4,1.5,0.4,setosa
    5.4,3.9,1.3,0.4,setosa
    5.1,3.5,1.4,0.3,setosa
    5.7,3.8,1.7,0.3,setosa
    5.1,3.8,1.5,0.3,setosa
    5.4,3.4,1.7,0.2,setosa
    5.1,3.7,1.5,0.4,setosa
    4.6,3.6,1.0,0.2,setosa
    5.1,3.3,1.7,0.5,setosa
    4.8,3.4,1.9,0.2,setosa
    5.0,3.0,1.6,0.2,setosa
    5.0,3.4,1.6,0.4,setosa
    5.2,3.5,1.5,0.2,setosa
    5.2,3.4,1.4,0.2,setosa
    4.7,3.2,1.6,0.2,setosa
    4.8,3.1,1.6,0.2,setosa
    5.4,3.4,1.5,0.4,setosa
    5.2,4.1,1.5,0.1,setosa
    5.5,4.2,1.4,0.2,setosa
    4.9,3.1,1.5,0.1,setosa
    5.0,3.2,1.2,0.2,setosa
    5.5,3.5,1.3,0.2,setosa
    4.9,3.1,1.5,0.1,setosa
    4.4,3.0,1.3,0.2,setosa
    5.1,3.4,1.5,0.2,setosa
    5.0,3.5,1.3,0.3,setosa
    4.5,2.3,1.3,0.3,setosa
    4.4,3.2,1.3,0.2,setosa
    5.0,3.5,1.6,0.6,setosa
    5.1,3.8,1.9,0.4,setosa
    4.8,3.0,1.4,0.3,setosa
    5.1,3.8,1.6,0.2,setosa
    4.6,3.2,1.4,0.2,setosa
    5.3,3.7,1.5,0.2,setosa
    5.0,3.3,1.4,0.2,setosa
    7.0,3.2,4.7,1.4,versicolor
    6.4,3.2,4.5,1.5,versicolor
    6.9,3.1,4.9,1.5,versicolor
    5.5,2.3,4.0,1.3,versicolor
    6.5,2.8,4.6,1.5,versicolor
    5.7,2.8,4.5,1.3,versicolor
    6.3,3.3,4.7,1.6,versicolor
    4.9,2.4,3.3,1.0,versicolor
    6.6,2.9,4.6,1.3,versicolor
    5.2,2.7,3.9,1.4,versicolor
    5.0,2.0,3.5,1.0,versicolor
    5.9,3.0,4.2,1.5,versicolor
    6.0,2.2,4.0,1.0,versicolor
    6.1,2.9,4.7,1.4,versicolor
    5.6,2.9,3.6,1.3,versicolor
    6.7,3.1,4.4,1.4,versicolor
    5.6,3.0,4.5,1.5,versicolor
    5.8,2.7,4.1,1.0,versicolor
    6.2,2.2,4.5,1.5,versicolor
    5.6,2.5,3.9,1.1,versicolor
    5.9,3.2,4.8,1.8,versicolor
    6.1,2.8,4.0,1.3,versicolor
    6.3,2.5,4.9,1.5,versicolor
    6.1,2.8,4.7,1.2,versicolor
    6.4,2.9,4.3,1.3,versicolor
    6.6,3.0,4.4,1.4,versicolor
    6.8,2.8,4.8,1.4,versicolor
    6.7,3.0,5.0,1.7,versicolor
    6.0,2.9,4.5,1.5,versicolor
    5.7,2.6,3.5,1.0,versicolor
    5.5,2.4,3.8,1.1,versicolor
    5.5,2.4,3.7,1.0,versicolor
    5.8,2.7,3.9,1.2,versicolor
    6.0,2.7,5.1,1.6,versicolor
    5.4,3.0,4.5,1.5,versicolor
    6.0,3.4,4.5,1.6,versicolor
    6.7,3.1,4.7,1.5,versicolor
    6.3,2.3,4.4,1.3,versicolor
    5.6,3.0,4.1,1.3,versicolor
    5.5,2.5,4.0,1.3,versicolor
    5.5,2.6,4.4,1.2,versicolor
    6.1,3.0,4.6,1.4,versicolor
    5.8,2.6,4.0,1.2,versicolor
    5.0,2.3,3.3,1.0,versicolor
    5.6,2.7,4.2,1.3,versicolor
    5.7,3.0,4.2,1.2,versicolor
    5.7,2.9,4.2,1.3,versicolor
    6.2,2.9,4.3,1.3,versicolor
    5.1,2.5,3.0,1.1,versicolor
    5.7,2.8,4.1,1.3,versicolor
    6.3,3.3,6.0,2.5,virginica
    5.8,2.7,5.1,1.9,virginica
    7.1,3.0,5.9,2.1,virginica
    6.3,2.9,5.6,1.8,virginica
    6.5,3.0,5.8,2.2,virginica
    7.6,3.0,6.6,2.1,virginica
    4.9,2.5,4.5,1.7,virginica
    7.3,2.9,6.3,1.8,virginica
    6.7,2.5,5.8,1.8,virginica
    7.2,3.6,6.1,2.5,virginica
    6.5,3.2,5.1,2.0,virginica
    6.4,2.7,5.3,1.9,virginica
    6.8,3.0,5.5,2.1,virginica
    5.7,2.5,5.0,2.0,virginica
    5.8,2.8,5.1,2.4,virginica
    6.4,3.2,5.3,2.3,virginica
    6.5,3.0,5.5,1.8,virginica
    7.7,3.8,6.7,2.2,virginica
    7.7,2.6,6.9,2.3,virginica
    6.0,2.2,5.0,1.5,virginica
    6.9,3.2,5.7,2.3,virginica
    5.6,2.8,4.9,2.0,virginica
    7.7,2.8,6.7,2.0,virginica
    6.3,2.7,4.9,1.8,virginica
    6.7,3.3,5.7,2.1,virginica
    7.2,3.2,6.0,1.8,virginica
    6.2,2.8,4.8,1.8,virginica
    6.1,3.0,4.9,1.8,virginica
    6.4,2.8,5.6,2.1,virginica
    7.2,3.0,5.8,1.6,virginica
    7.4,2.8,6.1,1.9,virginica
    7.9,3.8,6.4,2.0,virginica
    6.4,2.8,5.6,2.2,virginica
    6.3,2.8,5.1,1.5,virginica
    6.1,2.6,5.6,1.4,virginica
    7.7,3.0,6.1,2.3,virginica
    6.3,3.4,5.6,2.4,virginica
    6.4,3.1,5.5,1.8,virginica
    6.0,3.0,4.8,1.8,virginica
    6.9,3.1,5.4,2.1,virginica
    6.7,3.1,5.6,2.4,virginica
    6.9,3.1,5.1,2.3,virginica
    5.8,2.7,5.1,1.9,virginica
    6.8,3.2,5.9,2.3,virginica
    6.7,3.3,5.7,2.5,virginica
    6.7,3.0,5.2,2.3,virginica
    6.3,2.5,5.0,1.9,virginica
    6.5,3.0,5.2,2.0,virginica
    6.2,3.4,5.4,2.3,virginica
    5.9,3.0,5.1,1.8,virginica"""

    df = pandas.read_csv(StringIO(iris))

scatter_matrix(df,group_col='class')
plt.show()
