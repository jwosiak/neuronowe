import numpy as np
from scipy import stats
from sklearn import datasets

def KNN(point, k, X, targets):
    Y = X - point
    Y = np.sqrt(np.sum(Y * Y, axis=1))
    Y = np.array(zip(Y, targets), dtype=[('x', float), ('y', int)])
    Y.sort(order='x')
    Y = zip(*Y)[1]
    return int(stats.mode(Y[:k])[0][0])


import random as rnd

iris = datasets.load_iris()
testSize = 50

# weights = np.mean(iris.data, axis=0)
# m = np.max(weights)
# weights = (np.ones(4, dtype=float) / weights) * m
# print weights
#
# iris.data *= weights
# print iris.data


attempts = 500
errors = np.zeros(20, dtype=float)
for x in range(0, attempts):

    allIndices = np.arange(iris.data.shape[0])

    testSamples = np.array(rnd.sample(allIndices, testSize))
    trainSamples = np.array(list(set(allIndices) - set(testSamples)))
    for k in np.arange(1, 21, 2):
        # print "k:", k
        for point in testSamples:
            target = KNN(iris.data[point], k, iris.data[trainSamples], iris.target[trainSamples])
            # print point, target, iris.target[point]
            if target != iris.target[point]:
                errors[k] += 1
    print 'Done', 100 * x / (attempts-1), '%'
results = errors[np.arange(1,21,2)] / (testSize * attempts)

import matplotlib.pyplot as plt

plt.plot(np.arange(1,21,2), results, 'bx', np.arange(1,21,2), results, 'b')
plt.show()
